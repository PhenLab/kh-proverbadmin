@extends('admin.layouts.dashboard')

<?php

	$comment_type= '';
	if (@$type == 1){
		$comment_type = 'Daily Comments';
	}else if (@$type == 2){
		$comment_type = 'Weekly Comments';

	}else if (@$type == 3){
		$comment_type = 'Monthly Comments';

	}else if (@$type == 4){
		$comment_type = 'Yearly Comments';

	}else {
		$comment_type = 'All Comments';

	}
	?>
@section('page_heading',$comment_type)

@section('section')
<div class="col-sm-12">
<div class="row">
	<div class="col-sm-12 table-responsive">
		<table class="table table-bordered">
			<thead>
			<tr >
				<th class="text-center">Name</th>
				<th class="text-center">Email</th>
				<th class="text-center">Address</th>
			</tr>
			</thead>
			<tbody>
			<tr class="success">
				<td>John</td>
				<td>john@gmail.com</td>
				<td>London, UK</td>
			</tr>
			<tr>
				<td>Wayne</td>
				<td>wayne@gmail.com</td>
				<td>Manchester, UK</td>
			</tr>
			<tr class="info">
				<td>Andy</td>
				<td>andy@gmail.com</td>
				<td>Merseyside, UK</td>
			</tr>
			<tr>
				<td>Danny</td>
				<td>danny@gmail.com</td>
				<td>Middlesborough, UK</td>
			</tr>
			<tr class="warning">
				<td>Frank</td>
				<td>frank@gmail.com</td>
				<td>Southampton, UK</td>
			</tr>
			<tr>
				<td>Scott</td>
				<td>scott@gmail.com</td>
				<td>Newcastle, UK</td>
			</tr>
			<tr class="danger">
				<td>Rickie</td>
				<td>rickie@gmail.com</td>
				<td>Burnley, UK</td>
			</tr>
			</tbody>
		</table>


	</div>
</div>
</div>
@stop